let win = 0;
let count = 0
let curPlayer = "one"
let board = [
[0,0,0,0,0,0],
[0,0,0,0,0,0],
[0,0,0,0,0,0],
[0,0,0,0,0,0],
[0,0,0,0,0,0],
[0,0,0,0,0,0],
[0,0,0,0,0,0]]
function makeMove(event){
    count ++
    if(win === 1) 
        return
    let curCol = event.currentTarget
    let childCount = curCol.childElementCount
    if (childCount < 6){
        if (curPlayer === "one"){
            const oneDisk = document.createElement("div")
            oneDisk.classList.add("player1")
            curCol.appendChild(oneDisk)
            board[curCol.dataset.col][childCount] = 1
            curPlayer = "two"
        }
        else if (curPlayer === "two"){
            const twoDisk = document.createElement("div")
            twoDisk.classList.add("player2")
            curCol.appendChild(twoDisk)
            board[curCol.dataset.col][childCount] = 2
            curPlayer = "one"
        }
   
    }
    winCon()
   
}
function checkLine(a, b, c, d) {
    return ((a != 0) && (a == b) && (a == c) && (a == d))
}
function winCon (){
    if (count === 42){
        alert ("tie")
        win - 1;
    }
    for(let col = 0; col < board.length; col ++){
        for(let slot = 0; slot < board[col].length; slot ++){
            if (checkLine(board[col][slot], board[col][slot + 1], board[col][slot + 2], board[col][slot + 3])){
               if (board[col][slot] === 1){
                   alert("player 1 wins!")
                   win = 1;
               }
               else{
                   alert("player 2 wins")
                   win = 1;
               }
            }
        }
    }
    for (let col = 0; col < board.length - 3; col ++){
        for(let slot = 0; slot < board[col].length; slot ++){
            if (checkLine(board[col][slot], board[col + 1][slot], board[col + 2][slot], board[col + 3][slot])){
                if (board[col][slot] === 1){
                    alert("player 1 wins!")
                    win = 1;
                }
                else{
                    alert("player 2 wins")
                    win = 1;
                }
             }
        }
    }
    for (let col = 0; col < board.length - 4; col ++){
        for(let slot = 0; slot < board[col].length ; slot ++){
            if (checkLine(board[col][slot], board[col + 1][slot + 1], board[col + 2][slot + 2], board[col + 3][slot + 3])){
                if (board[col][slot] === 1){
                    alert("player 1 wins!")
                    win = 1;
                }
                else{
                    alert("player 2 wins")
                    win = 1;
                }
             }
        }
    }
    for (let col = 3; col < board.length; col ++){
        for(let slot = 0; slot < board[col].length - 4; slot ++){
            if (checkLine(board[col][slot], board[col - 1][slot + 1], board[col - 2][slot + 2], board[col - 3][slot + 3])){
                if (board[col][slot] === 1){
                    alert("player 1 wins!")
                    win = 1;
                }
                else{
                    alert("player 2 wins")
                    win = 1;
                }
             }
        }
    }
}

for (let columns of document.querySelectorAll(".column")){
       columns.addEventListener("click", makeMove)
       console.log(win)
   
}